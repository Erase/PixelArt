 <?php
	/**
	 * PixelArt <3
	 *
	 * @author A Web Developer • Gardener • Cyborg <www.green-effect.fr> 
	 */

	/**
	 * Traite une image pour l'afficher ensuite en pixel art HTML/table
	 */
	class PixelArt{	
		/**
		 * Image que l'on va traiter
		 * @var string
		 */
		private $image;

		/*
		 * Précision de l'image de sortie
		 * @var int
		 */
		public $precision;

		/*
		 * Active ou non le cache
		 * @var bol
		 */
		public $cache;
		
		/**
		 * Initialise une instance pour traiter une nouvelle image
		 * @param string $image
		 * @throws \Exception
		 */
		public function __construct($image, $precision = 1, $cache = false){
			//~ On vérifie que l'image existe avant de lancer le traitement
			if(file_exists($image)){
				$this->image = $image;
				$this->precision = $precision;
				$this->cache = $cache;
			}else{
				//~ Sinon on balance une jolie Exception <3
				throw new Exception('Image introuvable - Vérifiez le chemin');
			}
		}

		/**
		 * Traîte une image et l'affiche
		 * @throws \Exception
		 */
		public function render(){
			//~ Allo le répertoire "cache" ?
			if($this->cache && !is_dir('cache')){
				throw new Exception('Répertoire "cache" introuvable');
			}

			//~ Rapid' verif que le répertoire soit inscriptible
			if($this->cache && !is_writable('cache')){
				throw new Exception('Problème de droits d\'écriture sur le répertoire "cache"');
			}

			//~ Chemin du cache 
			$this->image_cache = 'cache/'.$this->image.'.html';

			//~ Cache valable une heure
			$expire = time() - 60 * 60 ; 

			//~ Cache ou pas cache ?
			if($this->cache && file_exists($this->image_cache) && filemtime($this->image_cache) > $expire){
				//~ Yeah babby ! Un ptit cache ! 
				readfile($this->image_cache);
			}else{
				//~ C'est parti \o/
				
				//~ On récupère la dimension de l'image
				list($width, $height, $type, $attr) = getimagesize($this->image);

				//~ On stocke en tableau parce que - merde, les tableaux, c'est top
				$_image[$this->image] = array(
					'nom'    =>	$this->image,
					'width'  =>	$width,
					'height' =>	$height,
					'objet'  =>	imagecreatefromgif($this->image)
				);
				
				//~ Parcours de tous les pixels pour extraire couleur et position
				$data_hexa = array();
				for($y=0; $y< $_image[$this->image]['height']; $y=$y+$this->precision){
					for($x=0; $x< $_image[$this->image]['width']; $x=$x+$this->precision){
						//~ Extraction couleur et repérage
						$index_of_pixel = imagecolorat($_image[$this->image]['objet'], $x, $y);
						$rvb_of_pixel   = imagecolorsforindex($_image[$this->image]['objet'], $index_of_pixel);
					
						//~ Une troisième boucle directement, ça fait plus "old school"
						$str_color = $rvb_of_pixel['red'].','.$rvb_of_pixel['green'].','.$rvb_of_pixel['blue'];
						array_push($data_hexa, array(
							'x'     =>	$x,
							'y'     =>	$y,
							'color' =>	self::convertColor($str_color)
						));
					}
				}

				$this->str_image = "";
				//~ Parcours des données traitées pour rendu HTML sur une bonne base en "table", à l'ancienne
				foreach($data_hexa as $col){
					$this->str_image .= '<td id="'.$col['x'].'-'.$col['y'].'" width="'.$this->precision.'" height="'.$this->precision.'" bgcolor="'.$col['color'].'"></td>';
					if($col['x'] + 1 == $_image[$this->image]['width']){
						$this->str_image .= '</tr><tr>';	//~ Saut de ligne
					}
				}

				$this->setInPageHTML();
				$str_page = $this->setImageInCache();
				echo $str_page;
			}
		}

		/**
		 * Balance une "image" en cache
		 * @throws \Exception
		 */
		protected function setImageInCache(){
			ob_start(); //~ Ouverture tampon
			echo $this->page;
			$str_page = ob_get_contents();
			ob_end_clean();
			if($this->cache){
				file_put_contents($this->image_cache, $str_page) ; //~ On écrit la chaîne précédemment récupérée ($page) dans un fichier ($cache) 
			}
			return $str_page;
		}

		/**
		 * Ajoute un contenu (string) à un ensemble HTML
		 */
		protected function setInPageHTML(){
			//~ Le code HTML contenant notre image sera... minimaliste
			$this->page = '<html><head></head><body><table style="border-collapse:collapse;border:0" cellpadding="0" cellspacing="0"><tr>';
			
			//~ Image au format HTML
			$this->page .= (empty($this->str_image) ? '## Aucune données extraite de l\'image ##' : $this->str_image);
			
			//~ Fin de notre talbeau et de notre page
			$this->page .= '</tr></table></body></html>';
		}

		/**
		 * Conversion de couleur hexa <=> RGB
		 * @param mixte $color (array || string)
		 * @return mixte couleur convertie 
		 */
		static function convertColor($color){
			//~ Conversion hexadecimal en RGB
			if(!is_array($color) && preg_match("/^[#]([0-9a-fA-F]{6})$/",$color)){
				$hex_R = substr($color,1,2);
				$hex_G = substr($color,3,2);
				$hex_B = substr($color,5,2);
				$RGB   = hexdec($hex_R).",".hexdec($hex_G).",".hexdec($hex_B);
				return $RGB;
			}
			//~ Conversion RGB en hexadecimal
			else{
				$hex_RGB = '';
				if(!is_array($color)){$color = explode(",",$color);}

				foreach($color as $value){
					$hex_value = dechex($value); 
					if(strlen($hex_value)<2){
						$hex_value="0".$hex_value;
					}
					$hex_RGB.= $hex_value;
				}
				return "#".$hex_RGB;
			}
		}
	}

	//~ Ca roule Raoul.
	$o_image = new PixelArt('image.gif');
	$o_image->render();

?>
